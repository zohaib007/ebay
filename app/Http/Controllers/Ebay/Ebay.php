<?php

namespace App\Http\Controllers\Ebay;

/**
 * ebay Trading API
 */
class Ebay
{
	/**
	 * @var string $app_id
     * can be retreived from ebay account
	 */
	private $app_id;

	/**
	 * @var string $endpoint
     * for sandbox account : https://api.sandbox.ebay.com/ws/api.dll
     * for live account : https://api.ebay.com/ws/api.dll
	 */
    private $endpoint;

    /**
     * @var string $dev_id
     * can be retreived from ebay account
     */
    private $dev_id;

    /**
     * @var string $cert_id
     * can be retreived from ebay account
     */
    private $cert_id;

    /**
     * @var string $token
     * can be retreived from ebay account | generated at runtime
     */
    private $token;

    /**
     * @var string $ru_name
     * can be retreived from ebay account
     */
    private $ru_name;

    /**
     * @var array $headers
     * header for the API request
     */
    private $headers;

    /**
     * @var int $api_version
     */
    private $api_version;

    /**
     * @var string $session_id
     */
    private $session_id;

    /**
     * @var int $site_id
     */
    private $site_id;

    /**
     * @var array $sites
     */
    private $sites = array(
        'EBAY-US'    => 0,    // eBay United States       
        'EBAY-ENCA'  => 2,    // eBay Canada (English)    
        'EBAY-GB'    => 3,    // eBay United Kingdom      
        'EBAY-AU'    => 15,   // eBay Australia           
        'EBAY-AT'    => 16,   // eBay Austria             
        'EBAY-FRBE'  => 23,   // eBay Belgium (French)    
        'EBAY-FR'    => 71,   // eBay France              
        'EBAY-DE'    => 77,   // eBay Germany             
        'EBAY-MOTOR' => 100,  // eBay Motors              
        'EBAY-IT'    => 101,  // eBay Italy               
        'EBAY-NLBE'  => 123,  // eBay Belgium (Dutch)     
        'EBAY-NL'    => 146,  // eBay Netherlands         
        'EBAY-ES'    => 186,  // eBay Spain               
        'EBAY-CH'    => 193,  // eBay Switzerland         
        'EBAY-HK'    => 201,  // eBay Hong Kong           
        'EBAY-IN'    => 203,  // eBay India               
        'EBAY-IE'    => 205,  // eBay Ireland             
        'EBAY-MY'    => 207,  // eBay Malaysia            
        'EBAY-FRCA'  => 210,  // eBay Canada (French)     
        'EBAY-PH'    => 211,  // eBay Philippines         
        'EBAY-PL'    => 212,  // eBay Poland              
        'EBAY-SG'    => 216   // eBay Singapore           
    );

    /**
     * @var int $condition
     */
    private $condition;

    /**
     * @var array $conditions
     */
    private $conditions = array(
        'new' => 1000,
        'new_other' => 1500,
        'new_with_defects' => 1750,
        'manufacturer_refurbished' => 2000,
        'seller_refurbished' => 2500,
        'used' => 3000,
        'very_good' => 4000,
        'good' => 5000,
        'acceptable' => 6000,
        'for_parts_or_not_working' => 7000
    );

	/**
	 * __construct | ebay class constructor
	 * @return void()
	 */
	function __construct(){

		$this->api_version = 971;
		$this->site_id = $this->sites['EBAY-US'];
		$this->app_id = "QasimPur-Testproj-SBX-9820665a8-fc858bc5";
        $this->dev_id = "cb0d9b03-6594-4270-b471-ae2e77618ddc";
        $this->cert_id = "SBX-820665a85fe4-322c-45ea-83ad-150e";
        $this->ru_name = "Qasim_Puri-QasimPur-Testpr-cuogk";
		$this->token = "AgAAAA**AQAAAA**aAAAAA**oIMaXA**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4ajC5iDqQudj6x9nY+seQ**KrkEAA**AAMAAA**T3hkpcGyCt5iTRBiKOAZx20ru46tYWgZIMZ4jCMCBNIgfMB9rIyFw0A4qOLLZGP/7g2gsvot1Kp5wgREbU0nLpnjuL9+mtUnhEXXTXSe2PsLzZU3vJsP93VUVyUzYpTDasL2i488ZhDDkEdXtGC2o0Ve6BUC0X1M52ezDDvLz3vLIGSigxob+AG+nRJbUcbalxkzqD5CG1Re34Yx+g3uppz8Jjr2XTX0QGVxmvv6KYqcLDyp1DQNN5ZCCKZFpioux28vkH4j4rPsWYj+sAuTEsa5zSBX+pLHbbSo6V7a7W93wdu3GShNcL5qxzZ4frdPj12poDuvUOh7m+Gv8EtumorSal5vXsn1gmzIU8N+3Poyfk9MXWnU7r5cF+SiuxKf1kzGTFIwr63DKBtzx80SwgjH73YcZWQ4IkUs1pDxdyif3FM32OQiJ9BNf0HhQsz29vCFa+RDh9XXfJG2gr9A4kSbCOyO29mJqfssgqem47caOwji7jgnCR0j4cgwxNZ4EyWEe6KIIadvVTDdCcUyv+mkEs5XoGC5pRYCEVKELMgocLplbhlrZlnyJhsd+1zj73yruifdb+/muCfMN5NF9e8MAHbEtPdMMjw8fbVkatqAP6ajRD7siSbGQE4TDrQySyuYQzkk7cI1WAtJlxTPlr2YqNoINt6MnV0aw38golJecGELmama+irjFvaxNxPEGT7b+Tp+U4tAoz7nfhTPRSFrM6KrmSaLOYzcN3YaLhJ0mJOC8zh8MMYxu7pvk5tO";

        $this->endpoint = "https://api.sandbox.ebay.com/ws/api.dll";

        $this->condition = $this->conditions['new'];

        $this->headers = array(
                    'Content-Type: text/xml',
                    'X-EBAY-API-COMPATIBILITY-LEVEL: '.$this->api_version,
                    'X-EBAY-API-DEV-NAME: '.$this->dev_id,
                    'X-EBAY-API-APP-NAME: '.$this->app_id,
                    'X-EBAY-API-CERT-NAME: '.$this->cert_id,
                    'X-EBAY-API-SITEID: '.$this->site_id
        );
	}

    /**
     * getSessionID | get Session ID
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSessionID(){

        array_push($this->headers, 'X-EBAY-API-CALL-NAME: GetSessionID');

        $xml =  '<?xml version="1.0" encoding="utf-8"?>'.
                '<GetSessionIDRequest xmlns="urn:ebay:apis:eBLBaseComponents">'.
                '<RuName>'.$this->ru_name.'</RuName>'.
                '<ErrorLanguage>en_US</ErrorLanguage>'.
                '<Version>'.$this->api_version.'</Version>'.
                '<WarningLevel>High</WarningLevel>'.
                '</GetSessionIDRequest>';

        $result = $this->processRequest($xml);

        if($result['Ack']=="Success"){
            $this->session_id = $result['SessionID'];
            return response()->json($result);
        }else{
            return response()->json($result);
        }
    }

    /**
     * fetchToken | generates token at runtime
     * @return \Illuminate\Http\JsonResponse
     */
    public function fetchToken()
    {
        // get session ID to fetch token
        $sessionResponse = json_decode($this->getSessionID()->getContent());

        if($sessionResponse->Ack=="Success"){

            array_push($this->headers, 'X-EBAY-API-CALL-NAME: FetchToken');

            $xml =  '<?xml version="1.0" encoding="utf-8"?>'.
                    '<FetchTokenRequest xmlns="urn:ebay:apis:eBLBaseComponents">'.
                    '<RequesterCredentials>'.
                    '<eBayAuthToken>'.$this->token.'</eBayAuthToken>'.
                    '</RequesterCredentials>'.
                    '<ErrorLanguage>en_US</ErrorLanguage>'.
                    '<Version>'.$this->api_version.'</Version>'.
                    '<WarningLevel>High</WarningLevel>'.
                    '<SessionID>'.$this->session_id.'</SessionID>'.
                    '</FetchTokenRequest>';

            $result = $this->processRequest($xml);
            dd($result);
            if($result['Ack']=="Success"){
                $this->token = $result['SessionID'];
                return response()->json($result);
            }else{
                return response()->json($result);
            }
        }else{
            return response()->json($sessionResponse);
        }
    }

	/**
	 * getEbayOfficialTime | get ebay standard time
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function getEbayOfficialTime(){

        array_push($this->headers, 'X-EBAY-API-CALL-NAME: GeteBayOfficialTime');

        $xml =  '<?xml version="1.0" encoding="utf-8"?>'.
                '<GeteBayOfficialTimeRequest xmlns="urn:ebay:apis:eBLBaseComponents">'.
                '<RequesterCredentials>'.
                '<eBayAuthToken>'.$this->token.'</eBayAuthToken>'.
                '</RequesterCredentials>'.
                '<ErrorLanguage>en_US</ErrorLanguage>'.
                '<WarningLevel>High</WarningLevel>'.
                '</GeteBayOfficialTimeRequest>';
        
        $result = $this->processRequest($xml);

        return response()->json($result);
    }

    /**
     * getEbayCategories | get ebay categories and sub categories
     * @param int|null $parent_id 
     * @param int|null $category_level 
     * @return \Illuminate\Http\JsonResponse
     */
    public function getEbayCategories(int $parent_id=null,int $category_level=null){

        array_push($this->headers, 'X-EBAY-API-CALL-NAME: GetCategories');

        $xml =  '<?xml version="1.0" encoding="utf-8"?>'.
                '<GetCategoriesRequest xmlns="urn:ebay:apis:eBLBaseComponents">'.
                '<RequesterCredentials>'.
                '<eBayAuthToken>'.$this->token.'</eBayAuthToken>'.
                '</RequesterCredentials>';

                // retreive the childs of the provided parent category
                if($parent_id!=null){
                    //if $parent_id is not null retreive child categories
                    $xml .=  '<CategoryParent>'.$parent_id.'</CategoryParent>';
                }

        $xml .= '<CategorySiteID>'.$this->site_id.'</CategorySiteID>';

                if($category_level!=null){
                    $xml .= '<LevelLimit>'.($category_level + 1).'</LevelLimit>';
                }else{
                    $xml .= '<LevelLimit>1</LevelLimit>';
                }

        $xml .= '<ViewAllNodes>true</ViewAllNodes>'.
                '<DetailLevel>ReturnAll</DetailLevel>'.
                '<ErrorLanguage>en_US</ErrorLanguage>'.
                '<WarningLevel>High</WarningLevel>'.
                '</GetCategoriesRequest>';

        $result = $this->processRequest($xml);

        return response()->json($result);
    }

    /**
     * addFixedPriceItem | adds a fixed priced item for ebay listing
     * @param array|null $prodData 
     * @return \Illuminate\Http\JsonResponse
     */
    public function addFixedPriceItem(array $prodData=null){

        array_push($this->headers, 'X-EBAY-API-CALL-NAME: AddFixedPriceItem');

        $xml =  '<?xml version="1.0" encoding="utf-8"?>'.
                '<AddFixedPriceItemRequest xmlns="urn:ebay:apis:eBLBaseComponents">'.
                '<RequesterCredentials>'.
                '<eBayAuthToken>'.$this->token.'</eBayAuthToken>'.
                '</RequesterCredentials>'.
                '<ErrorLanguage>en_US</ErrorLanguage>'.
                '<WarningLevel>High</WarningLevel>'.
                '<Item>'.
                '<Title>Test Product 2</Title>'.
                '<Description>Test Product Description</Description>'.
                '<PrimaryCategory>'.
                '<CategoryID>111422</CategoryID>'.
                '</PrimaryCategory>'.
                '<StartPrice>500.0</StartPrice>'.
                '<ConditionID>'.$this->condition.'</ConditionID>'.
                '<Country>US</Country>'.
                '<Currency>USD</Currency>'.
                '<DispatchTimeMax>3</DispatchTimeMax>'.
                '<ListingDuration>GTC</ListingDuration>'.
                '<ListingType>FixedPriceItem</ListingType>'.
                '<PaymentMethods>PayPal</PaymentMethods>'.
                '<PaymentMethods>VisaMC</PaymentMethods>'.
                '<PaymentMethods>AmEx</PaymentMethods>'.
                '<PaymentMethods>Discover</PaymentMethods>'.
                '<PayPalEmailAddress>megaonlinemerchant@gmail.com</PayPalEmailAddress>'.
                '<PictureDetails>'.
                '<GalleryType>Gallery</GalleryType>'.
                '</PictureDetails>'.
                '<PostalCode>95125</PostalCode>'.
                '<ProductListingDetails>'.
                '<UPC>885909298594</UPC>'.
                '<IncludeStockPhotoURL>true</IncludeStockPhotoURL>'.
                '<IncludeeBayProductDetails>true</IncludeeBayProductDetails>'.
                '<UseFirstProduct>true</UseFirstProduct>'.
                '<UseStockPhotoURLAsGallery>true</UseStockPhotoURLAsGallery>'.
                '<ReturnSearchResultOnDuplicates>true</ReturnSearchResultOnDuplicates>'.
                '</ProductListingDetails>'.
                '<Quantity>6</Quantity>'.
                '<ReturnPolicy>'.
                '<ReturnsAcceptedOption>ReturnsAccepted</ReturnsAcceptedOption>'.
                '<RefundOption>MoneyBack</RefundOption>'.
                '<ReturnsWithinOption>Days_30</ReturnsWithinOption>'.
                '<ShippingCostPaidByOption>Buyer</ShippingCostPaidByOption>'.
                '</ReturnPolicy>'.
                '<ShippingDetails>'.
                '<ShippingType>Flat</ShippingType>'.
                '<ShippingServiceOptions>'.
                '<ShippingServicePriority>1</ShippingServicePriority>'.
                '<ShippingService>UPSGround</ShippingService>'.
                '<FreeShipping>true</FreeShipping>'.
                '<ShippingServiceAdditionalCost currencyID="USD">0.00</ShippingServiceAdditionalCost>'.
                '</ShippingServiceOptions>'.
                '</ShippingDetails>'.
                '<Site>US</Site>'.
                '</Item>'.
                '</AddFixedPriceItemRequest>';

        $result = $this->processRequest($xml);

        return response()->json($result);
    }

    /**
     * reviseFixedPriceItem | update Fixed Price Item
     * @param string|null $prod_sku 
     * @param array $prodData 
     * @return \Illuminate\Http\JsonResponse
     */
    public function reviseFixedPriceItem(string $prod_sku=null,array $prodData=null)
    {
        array_push($this->headers, 'X-EBAY-API-CALL-NAME: ReviseFixedPriceItem');

        $xml =  '<?xml version="1.0" encoding="utf-8"?>'.
                '<ReviseFixedPriceItemRequest xmlns="urn:ebay:apis:eBLBaseComponents">'.
                '<RequesterCredentials>'.
                '<eBayAuthToken>'.$this->token.'</eBayAuthToken>'.
                '</RequesterCredentials>'.
                '<ErrorLanguage>en_US</ErrorLanguage>'.
                '<WarningLevel>High</WarningLevel>'.
                '<Item>'.
                '<ItemID>110389454166</ItemID>'.
                '<Title>Test Product 2 revised</Title>'.
                '<Description>Test Product Description</Description>'.
                '<PrimaryCategory>'.
                '<CategoryID>111422</CategoryID>'.
                '</PrimaryCategory>'.
                '<StartPrice>100.0</StartPrice>'.
                '<ConditionID>'.$this->condition.'</ConditionID>'.
                '<Country>US</Country>'.
                '<Currency>USD</Currency>'.
                '<DispatchTimeMax>3</DispatchTimeMax>'.
                '<ListingDuration>GTC</ListingDuration>'.
                '<ListingType>FixedPriceItem</ListingType>'.
                '<PaymentMethods>PayPal</PaymentMethods>'.
                '<PaymentMethods>VisaMC</PaymentMethods>'.
                '<PaymentMethods>AmEx</PaymentMethods>'.
                '<PaymentMethods>Discover</PaymentMethods>'.
                '<PayPalEmailAddress>megaonlinemerchant@gmail.com</PayPalEmailAddress>'.
                '<PictureDetails>'.
                '<GalleryType>Gallery</GalleryType>'.
                '</PictureDetails>'.
                '<PostalCode>95125</PostalCode>'.
                '<ProductListingDetails>'.
                '<UPC>885909298594</UPC>'.
                '<IncludeStockPhotoURL>true</IncludeStockPhotoURL>'.
                '<IncludeeBayProductDetails>true</IncludeeBayProductDetails>'.
                '<UseFirstProduct>true</UseFirstProduct>'.
                '<UseStockPhotoURLAsGallery>true</UseStockPhotoURLAsGallery>'.
                '<ReturnSearchResultOnDuplicates>true</ReturnSearchResultOnDuplicates>'.
                '</ProductListingDetails>'.
                '<Quantity>6</Quantity>'.
                '<ReturnPolicy>'.
                '<ReturnsAcceptedOption>ReturnsAccepted</ReturnsAcceptedOption>'.
                '<RefundOption>MoneyBack</RefundOption>'.
                '<ReturnsWithinOption>Days_30</ReturnsWithinOption>'.
                '<ShippingCostPaidByOption>Buyer</ShippingCostPaidByOption>'.
                '</ReturnPolicy>'.
                '<ShippingDetails>'.
                '<ShippingType>Flat</ShippingType>'.
                '<ShippingServiceOptions>'.
                '<ShippingServicePriority>1</ShippingServicePriority>'.
                '<ShippingService>UPSGround</ShippingService>'.
                '<FreeShipping>true</FreeShipping>'.
                '<ShippingServiceAdditionalCost currencyID="USD">0.00</ShippingServiceAdditionalCost>'.
                '</ShippingServiceOptions>'.
                '</ShippingDetails>'.
                '<Site>US</Site>'.
                '</Item>'.
                '</ReviseFixedPriceItemRequest>';

        $result = $this->processRequest($xml);

        return response()->json($result);
    }

    /**
     * reviseQuantity | updates product quantity
     * @param string|null $prod_sku 
     * @param int|null $quantity 
     * @return \Illuminate\Http\JsonResponse
     */
    public function reviseQuantity(string $prod_sku=null,int $quantity=null)
    {
        array_push($this->headers, 'X-EBAY-API-CALL-NAME: ReviseInventoryStatus');

        $xml =  '<?xml version="1.0" encoding="utf-8"?>'.
                '<ReviseInventoryStatusRequest xmlns="urn:ebay:apis:eBLBaseComponents">'.
                '<RequesterCredentials>'.
                '<eBayAuthToken>'.$this->token.'</eBayAuthToken>'.
                '</RequesterCredentials>'.
                '<ErrorLanguage>en_US</ErrorLanguage>'.
                '<WarningLevel>High</WarningLevel>'.
                '<InventoryStatus>'.
                '<ItemID>'.$prod_sku.'</ItemID>'.
                '<Quantity>'.$quantity.'</Quantity>'.
                '</InventoryStatus>'.
                '</ReviseInventoryStatusRequest>';

        $result = $this->processRequest($xml);
        
        return response()->json($result);
    }

    /**
     * endFixedPriceItem | end sale for Fixed Price Item
     * @param string|null $prod_sku
     * @return \Illuminate\Http\JsonResponse
     */
    public function endFixedPriceItem(string $prod_sku=null)
    {
        array_push($this->headers, 'X-EBAY-API-CALL-NAME: EndFixedPriceItem');

        $xml =  '<?xml version="1.0" encoding="utf-8"?>'.
                '<EndFixedPriceItemRequest xmlns="urn:ebay:apis:eBLBaseComponents">'.
                '<RequesterCredentials>'.
                '<eBayAuthToken>'.$this->token.'</eBayAuthToken>'.
                '</RequesterCredentials>'.
                '<ErrorLanguage>en_US</ErrorLanguage>'.
                '<WarningLevel>High</WarningLevel>'.
                '<ItemID>'.$prod_sku.'</ItemID>'.
                '<EndingReason>NotAvailable</EndingReason>'.
                '</EndFixedPriceItemRequest>';

        $result = $this->processRequest($xml);
        
        return response()->json($result);
    }

    /**
     * relistFixedPriceItem | relists Fixed Price Item
     * @param string|null $prod_sku
     * @return \Illuminate\Http\JsonResponse
     */
    public function relistFixedPriceItem(string $prod_sku=null)
    {
        array_push($this->headers, 'X-EBAY-API-CALL-NAME: RelistFixedPriceItem');

        $xml =  '<?xml version="1.0" encoding="utf-8"?>'.
                '<RelistFixedPriceItemRequest xmlns="urn:ebay:apis:eBLBaseComponents">'.
                '<RequesterCredentials>'.
                '<eBayAuthToken>'.$this->token.'</eBayAuthToken>'.
                '</RequesterCredentials>'.
                '<ErrorLanguage>en_US</ErrorLanguage>'.
                '<WarningLevel>High</WarningLevel>'.
                '<Item>'.
                '<ItemID>'.$prod_sku.'</ItemID>'.
                '</Item>'.
                '</RelistFixedPriceItemRequest>';

        $result = $this->processRequest($xml);
        
        return response()->json($result);
    }

    /**
     * processRequest | processes request using CURL
     * @param string|null $xml 
     * @return array
     */
    public function processRequest(string $xml=null):array{
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);        
        curl_setopt($ch, CURLOPT_TIMEOUT, 400);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $result = curl_exec($ch);

        curl_close($ch);

        try{
            $result = new \SimpleXMLElement($result);
            return (array)$result;
        }catch(\Exception $e){
            $result['code'] = 500;
            $result['message'] = "Error : ".$e->getMessage().". Please check your internet connection.";
            return $result;
        }
    }
}