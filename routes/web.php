<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',function(){
	return view('welcome');
});

Route::prefix('ebay')->group(function(){
	Route::get('/ebay-time','Ebay\Ebay@getEbayOfficialTime')->name('getEbayOfficialTime');
	Route::get('/ebay-categories/{cat_id?}/{level?}','Ebay\Ebay@getEbayCategories')->name('getEbayCategories');
	Route::get('/ebay-add-fixed-priced-item','Ebay\Ebay@addFixedPriceItem')->name('addFixedPriceItem');
	Route::get('/ebay-update-fixed-priced-item/{sku}/{prodData}','Ebay\Ebay@reviseFixedPriceItem')->name('reviseFixedPriceItem');
	Route::get('/ebay-end-fixed-priced-item/{sku}','Ebay\Ebay@endFixedPriceItem')->name('endFixedPriceItem');
	Route::get('/ebay-relist-fixed-priced-item/{sku}','Ebay\Ebay@relistFixedPriceItem')->name('relistFixedPriceItem');
	Route::get('/get-session-id','Ebay\Ebay@getSessionID')->name('getSessionID');
	Route::get('/fetch-token','Ebay\Ebay@fetchToken')->name('fetchToken');
	Route::get('/update-quantity/{sku}/{quantity}','Ebay\Ebay@reviseQuantity')->name('reviseQuantity');
});
